# Welcome to your Note repo

This repo contains (mostly) some quick notes via markdown.

# Structure

This repo will follow other interesting note taking apps have taken, and model
the project/notebook with the filesystem rather a database.

- Notebook (submodule)
- Section
- Pages

# Git integration

The master repo is controlling the notebooks. Each additional notebook would be
a submodule.
